// g++ -o tweet-collector tweetCollector.cpp -O3 $(pkg-config --cflags --libs gaml) -lpthread -lcppkafka -lrdkafka



#include "tweetCollectorParams.hh"
#include <cppkafka/cppkafka.h>
#include <iostream>
#include <sstream>

int main(int argc, char* argv[]) {
  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
  }
  tweetoscope::params::collector params(argv[1]);
  std::cout << std::endl
        << "Parameters : " << std::endl
        << "----------"    << std::endl
        << std::endl
        << params << std::endl
        << std::endl;

    // configuration du consumer
  cppkafka::Configuration config_consumer= {
    { "bootstrap.servers", params.kafka.brokers },
    { "auto.offset.reset", "earliest" },
	  { "group.id", "myOwnPrivateCppGroup" }
  };

  //Topic properties
  //Spécifier que properties prend deux partitions

//créer un consumer
  cppkafka::Consumer consumer(config_consumer);
  consumer.subscribe({params.topic.in});

//créer un producer
  cppkafka::Configuration config_producer = {
	  { "bootstrap.servers", params.kafka.brokers,  }
  };

  //un producer qui écrit sur cascades_series
  cppkafka::MessageBuilder builder_series {params.topic.out_series};
  cppkafka::Producer producer(config_producer);

  //un producer qui écrit  sur cascades_propreties  
  cppkafka::MessageBuilder builder_properties {params.topic.out_properties};


  std::map<tweetoscope::source::idf,tweetoscope::cascade::Processor> processors;
  std::map<std::string, tweetoscope::cascade::priority_queue::handle_type> key_handle; 
  std::vector<std::size_t> observation   = params.times.observation;
  tweetoscope::timestamp T_over = params.times.terminated;


  while(true){
    //lire du topic
    auto msg = consumer.poll();
    //consumer.commit();
    if( msg && ! msg.get_error() ) {
      tweetoscope::tweet twt;
      auto init_key = tweetoscope::cascade::idf(std::stoi(msg.get_key()));
      auto istr = std::istringstream(std::string(msg.get_payload()));
      istr >> twt;

      auto key = std::to_string(init_key);
      //on crée le processeur de la source s'il n'a pas déjà été crée
      if (processors.find(twt.source)==processors.end()){
         tweetoscope::cascade::Processor processor(twt);
         processors.insert(std::make_pair(twt.source,processor));
      }
      tweetoscope::cascade::Processor* processor = &processors.at(twt.source);
      if(twt.type=="tweet"){
        tweetoscope::cascade::refCascade cscd = tweetoscope::cascade::cascade_ptr(twt, key);

        processor->source_time = twt.time;
        tweetoscope::cascade::weakCascade weak_cscd = cscd;

        //add to priority queue
        auto location = processor->priorityqueue.push(cscd); 

        key_handle.insert(std::make_pair(key,location));
  
        //add to FIFOs
        for(auto T_obs : observation){
          processor->fifo[T_obs].push(weak_cscd); 
        };

        //add to symbol tabels
        processor->symbole_table.insert(std::make_pair(key,weak_cscd));
      }
      else{
        tweetoscope::cascade::weakCascade wk_cascade = processor->symbole_table[key];
        if (auto cscd = wk_cascade.lock(); cscd){
            cscd->cascade_update(twt,key);
            processor->source_time = twt.time;
            processor->priorityqueue.decrease(key_handle[key],cscd);
            }
        if(wk_cascade.use_count()>1){
            throw std::invalid_argument( "Cascade has too many shared pointers (> 1" );
        }
      }   

      //Partial Cascades
      tweetoscope::cascade::series series_to_send = processor->send_partial_cascade(observation);
      for(auto& msg_series : series_to_send){  
            builder_series.payload(msg_series); 
            try{
            producer.produce(builder_series);
            } 
            catch (const cppkafka::HandleException& e) {
            std::ostringstream ostr2;
            ostr2 << e.what();
            std::string error {ostr2.str()};
            if (error.compare("Queue full") != 0) { 
              std::chrono::milliseconds timeout(3000);	
              producer.flush(timeout);
              producer.produce(builder_series);
            } else {
            std::cout << "something went wrong: " << e.what() << std::endl;
                }
            }
      } 



      // Cascade Properties
      tweetoscope::cascade::properties to_send = processor->send_terminated_cascade(T_over,params.cascade.min_cascade_size);
      for(auto& msg_properties : to_send){
            builder_properties.payload(msg_properties); 
            int i = 0;
            for(auto T_obs : observation){
              auto T_obs_key = std::to_string(T_obs);
              builder_properties.partition(i);
              i++;
              builder_properties.key(T_obs_key);
              try{
              producer.produce(builder_properties);
              } 
              catch (const cppkafka::HandleException& e) {
              std::ostringstream ostr2;
              ostr2 << e.what();
              std::string error {ostr2.str()};
              if (error.compare("Queue full") != 0) { 
                std::chrono::milliseconds timeout(3000);	
                producer.flush(timeout);
                producer.produce(builder_properties);
              } else {
              std::cout << "something went wrong: " << e.what() << std::endl;
                  }
              }
            }
      }
    }
  };
  return 0;
}