#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Nina, Mélina & Zineb

Writing a stats text to use in V0 
"""



# External Import 
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
import pprint
import pandas as pd 
pp = pprint.PrettyPrinter(indent=4)
""" Reading from the topic tweets that has been generated"""



# Topics's name
topic_lecture="cascade_series" 

# Arguments to write to run the file in a terminal  "python3 CollectorSeriesTest.py --broker-list localhost:9092".
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
args = parser.parse_args()  # Parse arguments


###########################################################################
########## Creating the text file and reading from tweets topic ###########
###########################################################################



#Creating the consumer + Reading JSON Message
consumer_tweets = KafkaConsumer(topic_lecture,                       
                          bootstrap_servers = args.broker_list,                       
                          value_deserializer =lambda v: json.loads(v.decode('utf-8')),                      
                          auto_offset_reset="earliest",
                          group_id = "myOwnP17",
                          consumer_timeout_ms = 1000)


msg_list = []
for msg in consumer_tweets:
    msg_list.append([msg.value["cid"],msg.value["T_obs"]]) 

#pp.pprint(msg_list)

df = pd.DataFrame(msg_list,columns=['cid','T_obs'])
df_counted = df.groupby('cid')['T_obs'].value_counts(ascending = True)

print(df_counted.loc[df_counted>1])


