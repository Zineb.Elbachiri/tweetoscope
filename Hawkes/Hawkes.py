#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Nina, Mélina & Zineb

When receiving a new cascade from the cascade_series topic, 
an estimator must infer the parameters of the underlying Hawkes 
process, according to the MAP estimator (cf labwork). 
Once an estimator has estimated parameters for a cascade, it posts the 
result as a message of type parameters in the cascade_properties.
Here we use an exponential kernel parameterized by pair (p,β).

Ex of message sent by Hawkes :
Key = 600 Value = { 'type': 'parameters', 'cid': 'tw23981', 'msg' : 
    'blah blah', 'n_obs': 32, 'n_supp' : 120, 'params': [ 0.0423, 124.312 ],
    'n_etoile': n_etoile,'G1':G1}
"""

# External Import 
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producder
import numpy as np                # Import Numpy

# Internal Import
from fonction_support import prediction,compute_MAP # Estimation and prediction functions are written in fonction_support file.
import logger # To debug and follow what's done when the script is running.

###########################################################################
#################### Defining producers and consumers #####################
###########################################################################

# Topics's name
topic_lecture="cascade_series"  # Reading from
topic_ecriture="cascade_properties"  # Writting in


# Arguments to write to run the file in a terminal  "python3 Hawkes --broker-list localhost:9092".
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
args = parser.parse_args()  # Parse arguments


# Creating the consumer + Reading JSON Message
consumer = KafkaConsumer(topic_lecture,                       
                          bootstrap_servers = args.broker_list,                       
                          value_deserializer =lambda v: json.loads(v.decode('utf-8')),                      
                          auto_offset_reset="earliest",
                          group_id = "CascadeSeriesConsumerGroup")

# Creating the producer + Writting JSON Message
producer = KafkaProducer(
  bootstrap_servers = args.broker_list,                    
  value_serializer=lambda v: json.dumps(v).encode('utf-8'), 
  key_serializer=lambda v : json.dumps(v).encode('utf-8'))



###########################################################################
############################### Estimation ################################
###########################################################################

alpha, mu = 2.4, 10 # Parameters to model the magnitude distribution as a negative power law.

L_m=[]          # List of magnitudes to look at the real distribution and eventually compute a better fitting alpha & mu.

logger = logger.get_logger('Hawkes', broker_list=args.broker_list, debug=True) # Identify the node of origin of the message. 

# Reading cascade_series topic.
for msg in consumer:

    msg=msg.value
    
    # Getting data from msg
    T_obs=msg['T_obs']
    hist=msg['tweets']
    history=np.asarray(hist)
    n_obs=len(history)
    cid=msg['cid']
    message=msg['msg']
    
    # Getting the list of magnitude of this cascade to eventually look at the distribution of all magnitude
    L_m.append(history[:,1])
    
    logger.info("Reading tweets. T_obs is : " + str(T_obs) + " And cid is :" +str (cid)) # To know we are correctly reading cascade_series.
   
    # The origin of time of a cascade is set to 0.
    history[:,0] = history[:,0] - history[0][0]

    # Estimation
    res1,res2=compute_MAP(history, T_obs, alpha, mu)
    p=res2[0]
    beta=res2[1]
    params=[p,beta]
   
    # Prediction
    final_cascade_size=prediction(params, history, alpha, mu, T_obs)
    n_sup = round(final_cascade_size - n_obs)
    
    # Computing n_etoile
    n_etoile=p*mu*(alpha-1)/(alpha-2)
    
    # Computing G1
    I = history[:,0] < T_obs
    tis = history[I,0]
    mis = history[I,1]
    G1= p * np.sum(mis * np.exp(-beta * (T_obs - tis))) 

    # Preparing the message to send
    msg_to_send = {
        'type': 'params',
        'cid': cid,
        'msg': message,
        'n_obs': n_obs,
        'n_sup':n_sup,
        'params': params,
        'n_etoile': n_etoile,
        'G1':G1
        }
    
    # Sending a JSON message to the topic cascade_properties 
    logger.info("Sending message to cascade_properties")
    producer.send(topic_ecriture, key = T_obs, value = msg_to_send) 
producer.flush() # Flush: force purging intermediate buffers before leaving



