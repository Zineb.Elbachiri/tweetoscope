#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 16:53:47 2020

@author: Nina, Mélina & Zineb

We could use a fonction to avoid doing twice the same thing in the case we receive a params message and a size message.
"""


# External Import 
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producder
from kafka import TopicPartition
import pickle
import sys

# Internal Import
import logger


###########################################################################
#################### Defining producers and consumers #####################
###########################################################################

# Topics' name
properties_topic="cascade_properties" 
models_topic="models" 
samples_topic="samples"
alert_topic="alerts"
stat_topic="stats"

#topics'key
key_dic ={"300":0, "600":1, "1200":2}

# Arguments to write to run the file in a terminal  "python3 Hawkes --broker-list localhost:9092".
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
parser.add_argument('--obs-wind', type=str,required=True, help="the observation window : 300/600/1200")
args = parser.parse_args()  # Parse arguments

# Creating the consumer + decoding JSON Message
consumer_properties = KafkaConsumer(                             
                          bootstrap_servers = args.broker_list,                             # List of brokers passed from the command line
                          value_deserializer =lambda v: json.loads(v.decode('utf-8')),      # How to deserialize the value from a binary buffer
                          key_deserializer = lambda v: v.decode(),                          # How to deserialize the key (if any)
                          auto_offset_reset="earliest",                                     # Reading the topic from the beginning
                          group_id="PropertiesConsumerGroup-{}".format(args.obs_wind))      # Group's name

consumer_properties.assign([TopicPartition(properties_topic, key_dic[args.obs_wind])])


consumer_models = KafkaConsumer(models_topic,
                          bootstrap_servers = args.broker_list,              
                          value_deserializer =lambda m: pickle.loads(m),
                          auto_offset_reset="earliest",                               
                          group_id = "ModelsConsumerGroup-{}".format(args.obs_wind),                                     
                          consumer_timeout_ms=10000)   
# Creating the producer
producer = KafkaProducer(bootstrap_servers = args.broker_list,                              # List of brokers passed from the command line
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'),          # How to serialize the value to a binary buffer
                         key_serializer=  lambda v: json.dumps(v).encode('utf-8')           # How to serialize the key
                         )


##############################################################
#################### Reading from topics #####################
##############################################################

# To better follow the messages sent to the topic stats, samples and properties, we save them in text files.

are = open("/tmp/ARE.txt","a+")
pred=open("/tmp/pred.txt","a+")
properties=open("/tmp/properties.txt","a+")


# List to stock different messages depending on the topic
L_msg_properties_size=[]                                                    # To check 
L_msg_models=[]                                                             # List of forest, we always use the last one.
Dico_n_tot_true=dict()                                                      # Dictionnary of final size of cascades (usefull when we receive a params message and the corresponding msg size is already received).

L_when_to_send=[1,5,10,20,50,100]                                           # We decide when we compute a new forest.
L_msg_to_samples=[]                                                         # To know when to read the topic models, we have to know how many messages were sent to samples. We stock them in a list.

D_msg_waiting=dict()                                                        # Dictionnary of params messages (usefull when we receive a size message : if the corresponding params message was already sent we can proceed.

logger = logger.get_logger('Predictor', broker_list=args.broker_list, debug=True)  # Identify the node of origin of the message.

# Reading from cascade_properties topic.
for msg in consumer_properties:
                                            
    # For each message (size or params), we get T_obs, the message and the cid.
    T_obs=msg.key                                                          
    msg=msg.value                                                           
    cid=msg['cid']                                                         
    
    if msg['type']=='params':                                               # If it is a parameters message
        
        logger.info("Message of type parameters. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))
        
        # Getting the data from the message
        params=msg['params']                                                
        n_etoile=msg['n_etoile']                                            
        G1=msg['G1']                                                       
        n=msg['n_obs']                                                      
        
        if cid in Dico_n_tot_true:                                          # If the final size of the cascade is already received          
            n_tot_true = Dico_n_tot_true[cid]                               # Getting the final size
            X=[params[0],n_etoile,G1]                                       # Stocking the arguments of the forest in a vector X=[beta,n_etoile,G1]
            
            # Computing the true omega and dealing with G1=0 problem.
            try:
                w_true=(n_tot_true - n) * (1 - n_etoile) / G1 
            except ZeroDivisionError as err:
                logger.critical(" Division by zero ! T_obs : " + str(err))
                w_true=-1

            # Creating the sample message
            msg_to_samples = {                                              
                'type': 'samples',
                'cid': cid,
                'X': X,
                'w': w_true,                         
                }
            
            L_msg_to_samples.append(msg_to_samples)                         # Adding the message to the list of sample messages.
            producer.send(samples_topic, key = T_obs, value = msg_to_samples)# Sending the message to samples topic. 
            logger.info("Sending message to samples. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))

            if len(L_msg_to_samples) in L_when_to_send or len(L_msg_to_samples)%100==0 : # If there is enough samples sent, we read from model to get the latest forest compute
                for msg_model in consumer_models:                           # Getting the forest sent to the topic models.
                    L_msg_models.append(msg_model.value)
                    logger.info("Reading from topic models ")

            w_model = msg_model.value.predict([X])                          # We keep the latest forest and compute the prediction for the cascade we currently work on. 
            n_model = n + w_model[0] * ( G1/(1-n_etoile) )                  # Computing the final size of the cascade.
            
            
            # Computing ARE and dealing with the case ARE>1.
            ARE = abs(n_model - n_tot_true) / n_tot_true                    
            
            if ARE >1: 
                msg_to_stat={
                    'type': 'stat',
                    'cid': cid,
                    'T_obs': T_obs,
                    'ARE': ARE,
                    'n_model': n_model,
                    'n_true' : n_tot_true                         
                    }
                # Since the value is not what we expect, we give more details about the calcul in the message sent to stat.
                if len(L_msg_to_samples)<20:
                    are.write('{cascade:'+str(cid)+', T_obs: '+str(T_obs)+' , ARE: ' + "{:.5f}".format(ARE) + ' , n_model ' + str(round(n_model)) + ', n_true '+ str(n_tot_true) +' ,G1 '+ str(round(G1)) + '}\n')
                    logger.info("Wrote in ARE.txt")
                else : 
                    logger.info("Finished processing")
                    sys.exit()   
                    
            
            else:   # Otherwise we sent what was asked in stats topic.
                msg_to_stat={
                        'type': 'stat',
                        'cid': cid,
                        'T_obs': T_obs,
                        'ARE': ARE                       
                        }
                if len(L_msg_to_samples)<20:
                    are.write('{cascade:'+str(cid)+', T_obs: '+str(T_obs)+' , ARE: ' + "{:.5f}".format(ARE) + ' , n_model ' + str(round(n_model)) + ', n_true '+ str(n_tot_true) +' ,G1 '+ str(round(G1)) + '}\n')
                    logger.info("Wrote in ARE.txt")

                else : 
                    logger.info("Finished processing")
                    sys.exit() 
             
            msg_to_alert ={
                    'type' : 'alert',
                    'cid' :cid,
                    'msg' : 'blah blah',
                    'T_obs' : T_obs,
                    'n_tot' : n_model,
                    }
            
            # Keeping tracks of the messages in text files.
            pred.write('{cascade:'+str(cid)+', final size: '+str(round(n_model))+'}\n')
            properties.write('{type:params, id : '+str(cid)+', final size: '+str(round(n_model))+'\n')
            
            # Sendig the message to the topics stats and alerts.
            producer.send(alert_topic, key = None, value = msg_to_alert)    
            producer.send(stat_topic, key = None, value = msg_to_stat)      
            logger.info("Sending messages to alerts and stats. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))
            
            

                
        else: # If the cascade size isn't received yet, I keep the message params in a dictionnary wating to be process when the corresponding message size is received.
            D_msg_waiting[cid]= msg
            logger.debug("Final size isn't received yet. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))
            
    else:                                                                   # Si c'est un msg de type "size", je récupère le n_tot, que je stocke dans le dico avec comme clé le cid
       
        logger.info("Message of type size. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))
        L_msg_properties_size.append(msg)
        n_tot_true=msg['n_tot']
        Dico_n_tot_true[cid]=n_tot_true
        
        if cid in D_msg_waiting:
           
            msg_params=D_msg_waiting[cid]
            
            params=msg_params['params']                                    
            n_etoile=msg_params['n_etoile']
            G1=msg_params['G1']
            n=msg_params['n_obs']
            
            X=[params[0],n_etoile,G1]
            
            # Computing the true omega and dealing with G1=0 problem.
            try:
                w_true=(n_tot_true - n) * (1 - n_etoile) / G1 
            except ZeroDivisionError as err:
                logger.critical(" Division by zero ! T_obs : " + str(err))
                w_true=-1

            
            msg_to_samples = {
                'type': 'samples',
                'cid': cid,
                'X': X,
                'w': w_true,                         
                }
            
            L_msg_to_samples.append(msg_to_samples)                        
            producer.send(samples_topic, key = T_obs, value = msg_to_samples)
            logger.info("Sending messages to samples. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))

            if len(L_msg_to_samples) in L_when_to_send or len(L_msg_to_samples)%100==0:                     
                for msg_model in consumer_models:                           
                    L_msg_models.append(msg_model.value)

            
            w_model = msg_model.value.predict([X])                         
            n_model = n + w_model[0] * ( G1/(1-n_etoile) )                   
            ARE = abs(n_model - n_tot_true) / n_tot_true                   
            
            if ARE >1: 
                msg_to_stat={
                    'type': 'stat',
                    'cid': cid,
                    'T_obs': T_obs,
                    'ARE': ARE,
                    'n_model': n_model,
                    'n_true' : n_tot_true                         
                    }
                if len(L_msg_to_samples)<20:
                    are.write('{cascade:'+str(cid)+', T_obs: '+str(T_obs)+' , ARE: '+ "{:.5f}".format(ARE) + ' , n_model ' + str(round(n_model)) + ', n_true'+ str(n_tot_true)  +' ,G1 '+ str(G1) + '\n')
                    logger.info("Wrote in ARE.txt")
                else : 
                    logger.info("Finished processing")
                    sys.exit()     
            
            else:   
                msg_to_stat={
                        'type': 'stat',
                        'cid': cid,
                        'T_obs': T_obs,
                        'ARE': ARE                       
                        }
                if len(L_msg_to_samples)<20:
                    are.write('{cascade:'+str(cid)+', T_obs: '+str(T_obs)+' , ARE: ' + "{:.5f}".format(ARE) + ' , n_model ' + str(round(n_model)) + ', n_true '+ str(n_tot_true) +' ,G1 '+ str(round(G1)) + '}\n')
                    logger.info("Wrote in ARE.txt")
                else : 
                    logger.info("Finished processing")
                    sys.exit()     
           
            msg_to_alert ={
                    'type' : 'alert',
                    'cid' : cid ,
                    'msg' : 'blah blah',
                    'T_obs' : T_obs,
                    'n_tot' : n_model,
                    }

            pred.write('{cascade:'+str(cid)+', final size: '+str(round(n_model))+'}\n')
            properties.write('{type:size, id : '+str(cid)+', final size: '+str(round(n_model))+'}\n')
            logger.info("Wrote in properties.txt")
            
            producer.send(stat_topic, key = None, value = msg_to_stat)  
            producer.send(alert_topic, key = None, value = msg_to_alert)  
            logger.info("Sending messages to alerts and stats. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))

        else :
             logger.debug("Message of type parameters isn't received yet. T_obs is : " + str(T_obs) + " And cid is :" +str (cid))

logger.info("Finished processing")
    
producer.flush() 
sys.exit()                                       

